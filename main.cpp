/*---------------------------------------------------------*\
| E1.31 Receiver for OpenRGB                                |
|                                                           |
| Adam Honse (CalcProgrammer1)      6/15/2020               |
\*---------------------------------------------------------*/
#include <chrono>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <e131.h>

using namespace std::chrono_literals;

/*---------------------------------------------------------*\
| OpenRGB SDK Includes                                      |
\*---------------------------------------------------------*/
#include "OpenRGB.h"
#include "NetworkClient.h"
#include "RGBController.h"

/*---------------------------------------------------------*\
| OpenRGB SDK Variables                                     |
\*---------------------------------------------------------*/
std::vector<NetworkClient*> rgb_clients;
std::vector<RGBController*> rgb_controllers;
std::vector<unsigned int>   universe_to_controller;
std::vector<unsigned int>   universe_offset;

int main()
{
    int sockfd;
    e131_packet_t packet;
    e131_error_t error;
    uint8_t last_seq = 0x00;

    /*-----------------------------------------------------*\
    | Connect to local OpenRGB server on default port       |
    \*-----------------------------------------------------*/
    NetworkClient * rgb_client = new NetworkClient(rgb_controllers);

    rgb_client->SetIP("127.0.0.1");
    rgb_client->SetName("OpenRGB E1.31 Receiver");
    rgb_client->SetPort(OPENRGB_SDK_PORT);

    rgb_client->StartClient();

    rgb_clients.push_back(rgb_client);

    while(!rgb_client->GetOnline())
    {
        std::this_thread::sleep_for(1ms);
    }

    printf("Client connected with %d devices\r\n", rgb_controllers.size());

    /*-----------------------------------------------------*\
    | Create a socket for E1.31                             |
    \*-----------------------------------------------------*/
    sockfd = e131_socket();

    if(sockfd < 0)
    {
        printf("Socket error\r\n");
        return 0;
    }

    /*-----------------------------------------------------*\
    | Bind the socket to the default E1.31 port and join    |
    | multicast group for universe 1                        |
    \*-----------------------------------------------------*/
    if(e131_bind(sockfd, E131_DEFAULT_PORT) < 0)
    {
        printf("Bind error\r\n");
        return 0;
    }

    /*-----------------------------------------------------*\
    | Join one E1.31 universe per device                    |
    \*-----------------------------------------------------*/
    unsigned int universe_idx = 1;

    for(unsigned int controller_idx = 0; controller_idx < rgb_controllers.size(); controller_idx++)
    {
        unsigned int universes_for_controller = 1 + ((rgb_controllers[controller_idx]->leds.size() * 3) / 512);

        for(unsigned int controller_universe_idx = 0; controller_universe_idx < universes_for_controller; controller_universe_idx++)
        {
            universe_to_controller.push_back(controller_idx);
            universe_offset.push_back(controller_universe_idx);

            if(e131_multicast_join(sockfd, (universe_idx)) < 0)
            {
                printf("Join error\r\n");
                return 0;
            }
            printf("Joining multicast universe %d for device %s with %d LEDs (%d Channels)\r\n", universe_idx, rgb_controllers[controller_idx]->name.c_str(), rgb_controllers[controller_idx]->colors.size(), 3 * rgb_controllers[controller_idx]->colors.size());

            universe_idx++;
        }
    }

    /*-----------------------------------------------------*\
    | Loop to receive E1.31 packets                         |
    \*-----------------------------------------------------*/
    printf("Waiting for E1.31 packets...\r\n");

    while(1)
    {
        if(e131_recv(sockfd, &packet) < 0)
        {
            printf("Receive error\r\n");
            return 0;
        }

        if((error = e131_pkt_validate(&packet)) != E131_ERR_NONE)
        {
            printf("Validation error: %s\r\n", e131_strerror(error));
            continue;
        }

//        if(e131_pkt_discard(&packet, last_seq))
//        {
//            //printf("Warning: packet out of order received\r\n");
//            last_seq = packet.frame.seq_number;
//            continue;
//        }

        last_seq = packet.frame.seq_number;

        /*-------------------------------------------------*\
        | If received packet is from a valid universe,      |
        | update the corresponding device                   |
        \*-------------------------------------------------*/
        unsigned int universe = ntohs(packet.frame.universe) - 1;
        if(universe < universe_to_controller.size())
        {
            unsigned int controller_idx = universe_to_controller[universe];

            if(controller_idx < rgb_controllers.size())
            {
                unsigned int color_idx_offset = universe_offset[universe] * 170;
                unsigned int num_of_colors    = rgb_controllers[controller_idx]->colors.size();
                bool last_universe            = false;

                if((num_of_colors - color_idx_offset) > 170)
                {
                    num_of_colors = 170;
                }

                for(unsigned int color_idx = color_idx_offset; color_idx < num_of_colors; color_idx++)
                {
                    {
                        RGBColor e131_color = ToRGBColor(packet.dmp.prop_val[((color_idx - color_idx_offset) * 3) + 1],
                                                         packet.dmp.prop_val[((color_idx - color_idx_offset) * 3) + 2],
                                                         packet.dmp.prop_val[((color_idx - color_idx_offset) * 3) + 3]);

                        rgb_controllers[controller_idx]->colors[color_idx] = e131_color;

                        if(color_idx == rgb_controllers[controller_idx]->colors.size() - 1)
                        {
                            last_universe = true;
                        }
                    }
                }

                if(last_universe)
                {
                    rgb_controllers[controller_idx]->DeviceUpdateLEDs();
                }
            }
        }
    }

    return 0;
}
